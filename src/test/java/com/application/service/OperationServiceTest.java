package com.application.service;

import com.application.entrypoint.restcontroller.error.ClientNotFoundException;
import com.application.entrypoint.restcontroller.error.NoBalanceException;
import com.application.entrypoint.restcontroller.error.TransactionNotFound;
import com.application.persistance.entity.*;

import com.application.persistance.entity.type.AmountType;
import com.application.persistance.repository.AmountRepository;
import com.application.persistance.repository.ClientRepository;
import com.application.persistance.repository.OperationRepository;

import com.application.service.dto.OperationDTO;
import com.application.service.dto.TransactionDTO;
import com.application.service.impl.IOperationService;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.math.BigInteger;
import java.util.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class OperationServiceTest {

    private static final String REFUND = "Remboursement";
    public static final String TEST_OK = "test reussi";

    @Mock
    private OperationRepository operationRepository;

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private AmountRepository amountRepository;

    @InjectMocks
    private IOperationService service;

    @Test
    public void shouldBeFindClient() {
        String login = "toto";
        // create operation 1
        Operation operation1 = new Operation();
        operation1.setOperationName("test1");
        // create operation 2
        Operation operation2 = new Operation();
        operation2.setOperationName("test2");
        List<Operation> operationList = new ArrayList<>();
        // add operation
        operationList.add(operation1);
        operationList.add(operation2);
        //mock
        Mockito.when(operationRepository.findByBankAccount_Client_Login(login)).thenReturn(operationList);
        // launch
        List<OperationDTO> result = new ArrayList<>(service.findByClient(login));
        // verify
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(operation1.getOperationName(), result.get(0).getOperationName());
        Assert.assertEquals(operation2.getOperationName(), result.get(1).getOperationName());
    }

    @Test
    public void shouldBeNotFindClient() {
        String login = "toto";
        //mock
        Mockito.when(operationRepository.findByBankAccount_Client_Login(login)).thenReturn(new ArrayList<>());
        // launch
        List<OperationDTO> result = new ArrayList<>(service.findByClient(login));
        // verify
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void shouldBePaid() {
        final BigInteger transactionNumber = BigInteger.valueOf(12345);
        final Operation operation = createOperation(transactionNumber, REFUND, AmountType.DEBIT, true);
        Mockito.when(operationRepository.findByTransactionNumber(transactionNumber)).thenReturn(operation);
        boolean result = service.isPaid(transactionNumber);
        Mockito.verify(operationRepository, Mockito.times(1)).findByTransactionNumber(transactionNumber);
        Assert.assertTrue(result);
    }

    @Test
    public void shouldNotPaidBecauseOperationIsNotEnabled() {
        final BigInteger transactionNumber = BigInteger.valueOf(12345);
        final Operation operation = createOperation(transactionNumber, REFUND, AmountType.DEBIT, false);
        Mockito.when(operationRepository.findByTransactionNumber(transactionNumber)).thenReturn(operation);
        boolean result = service.isPaid(transactionNumber);
        Mockito.verify(operationRepository, Mockito.times(1)).findByTransactionNumber(transactionNumber);
        Assert.assertFalse(result);
    }

    @Test
    public void shouldNotPaidBecauseOperationIsNotDebit() {
        final BigInteger transactionNumber = BigInteger.valueOf(12345);
        final Operation operation = createOperation(transactionNumber, REFUND, AmountType.CREDIT, true);
        Mockito.when(operationRepository.findByTransactionNumber(transactionNumber)).thenReturn(operation);
        boolean result = service.isPaid(transactionNumber);
        Mockito.verify(operationRepository, Mockito.times(1)).findByTransactionNumber(transactionNumber);
        Assert.assertFalse(result);
    }

    @Test
    public void shouldBeNotPaidBecauseTransactionNotFound() {
        BigInteger transaction = BigInteger.valueOf(12345);
        try{
            Mockito.when(operationRepository.findByTransactionNumber(transaction)).thenReturn(null);
            service.isPaid(transaction);
        }catch (TransactionNotFound execption){
            Assert.assertTrue(TEST_OK,true);
            Mockito.verify(operationRepository, Mockito.times(1)).findByTransactionNumber(transaction);
        }
    }

    @Test
    public void shouldBeRefund() {
        final BigInteger transactionNumber = BigInteger.valueOf(12345);
        final Operation operation = createOperation(transactionNumber, REFUND, AmountType.CREDIT, true);
        Mockito.when(operationRepository.findByTransactionNumber(transactionNumber)).thenReturn(operation);
        boolean result = service.isRefund(transactionNumber);
        Mockito.verify(operationRepository, Mockito.times(1)).findByTransactionNumber(transactionNumber);
        Assert.assertTrue(result);
    }

    @Test
    public void shouldNotRefundBecauseOperationIsNotEnabled() {
        final BigInteger transactionNumber = BigInteger.valueOf(12345);
        final Operation operation = createOperation(transactionNumber, REFUND, AmountType.CREDIT, false);
        Mockito.when(operationRepository.findByTransactionNumber(transactionNumber)).thenReturn(operation);
        boolean result = service.isRefund(transactionNumber);
        Mockito.verify(operationRepository, Mockito.times(1)).findByTransactionNumber(transactionNumber);
        Assert.assertFalse(result);
    }

    @Test
    public void shouldNotRefundBecauseOperationIsNotCredit() {
        final BigInteger transactionNumber = BigInteger.valueOf(12345);
        final Operation operation = createOperation(transactionNumber, REFUND, AmountType.DEBIT, true);
        Mockito.when(operationRepository.findByTransactionNumber(transactionNumber)).thenReturn(operation);
        boolean result = service.isRefund(transactionNumber);
        Mockito.verify(operationRepository, Mockito.times(1)).findByTransactionNumber(transactionNumber);
        Assert.assertFalse(result);
    }

    @Test
    public void shouldBeNotRefundBecauseTransactionNotFound() {
        BigInteger transaction = BigInteger.valueOf(12345);
        try{
            Mockito.when(operationRepository.findByTransactionNumber(transaction)).thenReturn(null);
            service.isRefund(transaction);
        }catch (TransactionNotFound execption){
            Assert.assertTrue(TEST_OK,true);
            Mockito.verify(operationRepository, Mockito.times(1)).findByTransactionNumber(transaction);
        }
    }

    @Test
    public void shouldBeGetNumOfTransactions() {
        final BigInteger transactionNumber = BigInteger.valueOf(1234567801);
        // create operations
        Collection<Operation> operations = Collections.singleton(createOperation(transactionNumber, REFUND, AmountType.CREDIT, true));
        // mock
        Mockito.when(operationRepository.findByBankAccount_NumberAccountAndOperationName(transactionNumber, REFUND)).thenReturn(operations);
        // launch test
        List<BigInteger> numOfTransactions = new ArrayList<>(service.getNumOfTransactions(transactionNumber,REFUND));
        Assert.assertEquals(1, numOfTransactions.size());
        Assert.assertEquals(transactionNumber, numOfTransactions.get(0));
        Mockito.verify(operationRepository, Mockito.times(1)).findByBankAccount_NumberAccountAndOperationName(transactionNumber, REFUND);
    }

    @Test
    public void shouldBeNotCreditBankAccountBecauseClientNotFound() {
        // create transaction
        final TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setNumberAccount("1234567801");
        transactionDTO.setSum(100D);
        try{
            Mockito.when(clientRepository.findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()))).thenReturn(null);
            service.creditBankAccount(transactionDTO);
        }catch (ClientNotFoundException exception){
            Assert.assertTrue(TEST_OK,true);
            Mockito.verify(clientRepository, Mockito.times(1)).findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()));
        }
    }

    @Test
    public void shouldBeCreditBank() {
        // create transaction
        final TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setNumberAccount("1234567801");
        transactionDTO.setSum(100D);
        // create client
        Client client = createClient(100D);
        // create amoun
        Amount amount = createAmount(transactionDTO.getSum(), AmountType.CREDIT);
        // mock
        Mockito.when(clientRepository.findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()))).thenReturn(client);
        Mockito.when(amountRepository.save(Mockito.any(Amount.class))).thenReturn(amount);
        service.creditBankAccount(transactionDTO);
        // verify
        Mockito.verify(clientRepository, Mockito.times(1)).findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()));
    }

    @Test
    public void shouldBeNotDebitBankAccountBecauseClientNotFound() {
        // create transaction
        final TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setNumberAccount("1234567801");
        transactionDTO.setSum(100D);
        try{
            Mockito.when(clientRepository.findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()))).thenReturn(null);
            service.debitBankAccount(transactionDTO);
        }catch (ClientNotFoundException exception){
            Assert.assertTrue(TEST_OK,true);
            Mockito.verify(clientRepository, Mockito.times(1)).findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()));
        }
    }

    @Test
    public void shouldBeNotDebitBankAccountBecauseBalanceException() {
        // create transaction
        final TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setNumberAccount("1234567801");
        transactionDTO.setSum(100D);
        // create client
        Client client = createClient(0D);
        try{
            Mockito.when(clientRepository.findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()))).thenReturn(client);
            service.debitBankAccount(transactionDTO);
        }catch (NoBalanceException exception){
            Assert.assertTrue(TEST_OK,true);
            Mockito.verify(clientRepository, Mockito.times(1)).findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()));
        }
    }

    @Test
    public void shouldBeDebitBank() {
        // create transaction
        final TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setNumberAccount("1234567801");
        transactionDTO.setSum(100D);
        // create client
        Client client = createClient(100D);
        // create amoun
        Amount amount = createAmount(transactionDTO.getSum(), AmountType.DEBIT);
        // mock
        Mockito.when(clientRepository.findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()))).thenReturn(client);
        Mockito.when(amountRepository.save(Mockito.any(Amount.class))).thenReturn(amount);
        service.debitBankAccount(transactionDTO);
        // verify
        Mockito.verify(clientRepository, Mockito.times(1)).findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()));
    }

    private Operation createOperation(final BigInteger transactionNumber, final String operationName, final AmountType amountType, final boolean isEnabled) {
        final Operation operation = new Operation();
        operation.setEnabled(isEnabled);
        operation.setTransactionNumber(transactionNumber);
        operation.setCreationDate(new Date());
        operation.setOperationName(operationName);
        // create amount
        final Amount amount = new Amount();
        amount.setType(amountType);
        amount.setOperation(operation);
        operation.setAmount(amount);
        return operation;
    }

    private Client createClient(Double balance) {
        Client client = new Client();
        client.setFirstName("Rémy");
        client.setLastName("Halipré");
        // create bank account
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(balance);
        client.setBankAccount(bankAccount);
        return client;
    }

    private Amount createAmount(Double sum, AmountType amountType) {
        Amount amount = new Amount();
        amount.setSum(sum);
        amount.setType(amountType);
        return amount;
    }

}
