package com.application.service;

import com.application.entrypoint.restcontroller.error.ClientNotFoundException;
import com.application.persistance.entity.*;
import com.application.persistance.repository.ClientRepository;
import com.application.service.dto.ClientDTO;
import com.application.service.impl.IClientService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientServiceTest {
    private ExpectedException expect = ExpectedException.none();

    @Mock
    private ClientRepository repository;

    @InjectMocks
    private IClientService service;


    @Test
    public void shouldBeFindByTransaction() {
        final BigInteger transaction = BigInteger.valueOf(12345);
        //create client
        final Client client = createClient();
        //mock
        Mockito.when(repository.findByTransaction(transaction)).thenReturn(client);
        //call method tested
        final   ClientDTO result = service.findByTransaction(transaction);
        // verify
        Assert.assertEquals(client.getFirstName(), result.getFirstName());
        Assert.assertEquals(client.getLastName(), result.getLastName());
        Assert.assertEquals(client.getAgency().getCity(), result.getAgency().getCity());
        Assert.assertEquals(client.getAgency().getName(), result.getAgency().getName());
        Mockito.verify(repository, Mockito.times(1)).findByTransaction(transaction);
    }

    @Test
    public void shouldBeNotFindByTransactionBecauseClientNotFound() {
        final BigInteger transaction = BigInteger.valueOf(12345);
        //mock
        Mockito.when(repository.findByTransaction(transaction)).thenReturn(null);
        try{
            service.findByTransaction(transaction);
            Mockito.verify(repository, Mockito.times(1)).findByTransaction(transaction);
        }catch (ClientNotFoundException execption){
            Assert.assertTrue("test reussi",true);
        }
    }

    private Client createClient() {
        final Client client = new Client();
        client.setFirstName("Rémy");
        client.setLastName("Halipré");
        final  Agency agency = new Agency();
        agency.setCity("Roncq");
        agency.setName("Test");
        client.setAgency(agency);
        return client;
    }
}
