package com.application.service;

import com.application.persistance.entity.BankAccount;
import com.application.persistance.repository.BankAccountRepository;
import com.application.service.impl.IBankAccountService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BankAccountServiceTest {

    @Mock
    private BankAccountRepository bankAccountRepository;

    @InjectMocks
    private IBankAccountService bankAccountService;

    @Test
    public void shouldBeGetBalance() {
        final BigInteger numberAccount = BigInteger.valueOf(1234567801);
        final BankAccount bankAccount = createBankAccount(numberAccount);
        Mockito.when(bankAccountRepository.getBankAccountByNumberAccount(bankAccount.getNumberAccount())).thenReturn(bankAccount);
        Double result = bankAccountService.getBalanceByNumberAccount(numberAccount);
        // veirfy
        Assert.assertEquals(bankAccount.getBalance(), result);
        Mockito.verify(bankAccountRepository, Mockito.times(1)).getBankAccountByNumberAccount(numberAccount);
    }

    private BankAccount createBankAccount(BigInteger transaction) {
       final BankAccount bankAccount = new BankAccount();
       bankAccount.setNumberAccount(transaction);
       bankAccount.setBalance(80.0);
       bankAccount.setEnabled(true);
       return bankAccount;
    }
}
