package com.application.repository;

import com.application.persistance.entity.Operation;
import com.application.persistance.repository.OperationRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.util.Collection;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OperationRepositoryTest {

    @Autowired
    private OperationRepository operationRepository;

    @Test
    public void shouldBeFindByTransactionNumberNotNull() {
        final BigInteger transaction = BigInteger.valueOf(1234567801);
        final Operation operation = operationRepository.findByTransactionNumber(transaction);
        Assert.assertNotNull("La classe ne doit pas être null", operation);
    }

    @Test
    public void shouldBeFindByNumberAccountAndOperationNameNotNull() {
        final String  numberAccountString = "1478541452158";
        final BigInteger numberAccount = new BigInteger(numberAccountString);
        final Collection<Operation> operation = operationRepository.findByBankAccount_NumberAccountAndOperationName(numberAccount,"Remboursement Amazon");
        Assert.assertNotNull("La classe ne doit pas être null", operation);
    }

    @Test
    public void shouldBeFindByBankAccountByLogin() {
        final String login = "23456";
        final Collection<Operation> operation = operationRepository.findByBankAccount_Client_Login(login);
        Assert.assertNotNull("La classe ne doit pas être null", operation);
    }
}
