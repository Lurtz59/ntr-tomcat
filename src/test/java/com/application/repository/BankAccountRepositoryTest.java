package com.application.repository;

import com.application.persistance.entity.BankAccount;
import com.application.persistance.repository.BankAccountRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.math.BigInteger;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BankAccountRepositoryTest {

    @Autowired
    private BankAccountRepository repository;

    @Test
    public void shouldBeGetBalanceByNumberAccount() {
        final String  numberAccountString = "1478541452158";
        final BigInteger numberAccount = new BigInteger(numberAccountString);
        final BankAccount bankAccount = repository.getBankAccountByNumberAccount(numberAccount);
        Assert.assertNotNull("La classe ne doit pas être null", bankAccount);
        Assert.assertEquals(numberAccount, bankAccount.getNumberAccount());
    }
}
