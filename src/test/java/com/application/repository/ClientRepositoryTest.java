package com.application.repository;

import com.application.persistance.entity.Client;
import com.application.persistance.repository.ClientRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.math.BigInteger;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientRepositoryTest {

    @Autowired
    private ClientRepository repository;

    @Test
    public void shouldBeFindByLoginNotNull() {
        final Client client = repository.findByLogin("12345");
        Assert.notNull(client, "La classe ne doit pas être null");
    }

    @Test
    public void shouldBeFindByTransaction() {
        final String transactionNumberString = "1234567801";
        final BigInteger transactionNumber = new BigInteger(transactionNumberString);
        final Client client = repository.findByTransaction(transactionNumber);
        Assert.notNull(client, "La classe ne doit pas être null");
    }

    @Test
    public void shouldBeFindByNumberAccount() {
        final String numberAccountString = "1478541452158";
        final BigInteger numberAccount = new BigInteger(numberAccountString);
        final Client client = repository.findByBankAccount_NumberAccount(numberAccount);
        Assert.notNull(client, "La classe ne doit pas être null");
    }
}
