package com.application;

import com.application.persistance.entity.Amount;
import com.application.persistance.entity.BankAccount;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;

public class XmlDebitAccess {

    public static void main(String[] args) {

        try {
            String numberAccountString = "1478541452158";
            BigInteger numberAccount = new BigInteger(numberAccountString);

            BankAccount bankAccount = new BankAccount();
            bankAccount.setNumberAccount(numberAccount);
            Amount sum = new Amount();
            sum.setSum(50D);
            String request = "<transactionDTO>" +
                    "           <numberAccount>"+bankAccount.getNumberAccount()+"</numberAccount>" +
                    "           <sum>"+sum.getSum()+"</sum>" +
                    "         </transactionDTO>";


            URL url = new URL("http://localhost:8081/api/operation/debit");
            HttpURLConnection urlConnection;
            urlConnection = (HttpURLConnection) url.openConnection();


            urlConnection.setConnectTimeout(20000);
            urlConnection.setReadTimeout(20000);

            // Set DoOutput to true if you want to use URLConnection for output.
            // Default is false
            urlConnection.setDoOutput(true);

            urlConnection.setUseCaches(true);
            urlConnection.setRequestMethod("POST");

            // Set Headers
            urlConnection.setRequestProperty("Accept", "application/xml");
            urlConnection.setRequestProperty("Content-Type", "application/xml");

            // Write XML
            OutputStream outputStream = urlConnection.getOutputStream();
            byte[] b = request.getBytes("UTF-8");

            outputStream.write(b);
            outputStream.flush();
            outputStream.close();

            // Read XML
            InputStream inputStream = urlConnection.getInputStream();
            byte[] res = new byte[2048];
            int i = 0;
            StringBuilder response = new StringBuilder();
            while ((i = inputStream.read(res)) != -1) {
                response.append(new String(res, 0, i));
            }

            inputStream.close();

            System.out.println("XML Debit WORK");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
