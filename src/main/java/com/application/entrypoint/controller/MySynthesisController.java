package com.application.entrypoint.controller;

import com.application.entrypoint.configuration.PageHTML;
import com.application.entrypoint.configuration.PageURL;
import com.application.service.BankAccountService;
import com.application.service.ClientService;
import com.application.service.OperationService;
import com.application.service.dto.ClientDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MySynthesisController {

	@Autowired
	private ClientService clientService;

	@Autowired
	private OperationService operationService;

	@Autowired
	private BankAccountService bankAccountService;

	@RequestMapping(PageURL.MY_SYNTHESIS)
	public String mySynthesis(final Model model) {
		final ClientDTO clientDTO = clientService.getLoggedUser();
		model.addAttribute("user", clientDTO);
		model.addAttribute("operations", operationService.findByClient(clientDTO.getLogin()));
		model.addAttribute("balance", bankAccountService.getBalanceByNumberAccount(clientDTO.getBankAccount().getNumberAccount()));
		return PageHTML.MY_SYNTHESIS;
	}
}
