package com.application.entrypoint.controller;

import com.application.entrypoint.configuration.PageHTML;
import com.application.entrypoint.configuration.PageURL;
import com.application.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TransferController {

	@Autowired
	ClientService clientService;

	@RequestMapping(PageURL.TRANSFER)
	public String transfer(final Model model) {
		model.addAttribute("user", clientService.getLoggedUser());
		return PageHTML.TRANSFER;
	}
}
