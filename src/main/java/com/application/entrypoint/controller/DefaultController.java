package com.application.entrypoint.controller;

import com.application.entrypoint.configuration.PageURL;
import org.springframework.web.bind.annotation.RequestMapping;

public class DefaultController {

    @RequestMapping(PageURL.ALL)
    public String defaultUrl() {
        return "redirect:"+ PageURL.MY_SYNTHESIS;
    }
}
