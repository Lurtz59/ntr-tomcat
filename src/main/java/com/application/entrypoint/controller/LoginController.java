package com.application.entrypoint.controller;

import com.application.entrypoint.configuration.ErrorMessage;
import com.application.entrypoint.configuration.PageHTML;
import com.application.entrypoint.configuration.PageURL;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class LoginController {

    @RequestMapping(value = PageURL.LOGIN, method = RequestMethod.GET)
    public String login() {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return "redirect:"+ PageURL.MY_SYNTHESIS;
        }
        return PageHTML.LOGIN;
    }

    @RequestMapping(value =  PageURL.LOGIN_ERROR, method = RequestMethod.GET)
    public String login(final Model model) {
        model.addAttribute("errorMessage", ErrorMessage.BAD_CREDENTIAL);
        return PageHTML.LOGIN;
    }

    @RequestMapping(value=PageURL.LOGOUT, method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:"+ PageURL.LOGIN;
    }
}
