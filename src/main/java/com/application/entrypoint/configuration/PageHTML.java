package com.application.entrypoint.configuration;

public interface PageHTML {

    String MY_SYNTHESIS = "my-synthesis";
    String LOGIN = "login";
    String TRANSFER = "transfer";
    String REGISTRATION = "registration";
}
