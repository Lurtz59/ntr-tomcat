package com.application.entrypoint.configuration;

public interface ErrorMessage {

    String BAD_CREDENTIAL = "Login ou mot de passe incorrect";

    String CLIENT_NOT_FOUND = "Client non trouvé";
    String CLIENT_FIRST_NAME_IS_EMPTY = "Prénom vide";
    String CLIENT_LAST_NAME_IS_EMPTY  = "Nom vide";

    String TRANSACTION_NOT_FOUND = "Transaction(s) non trouvée(s)";
    String TRANSACTION_NOT_NUMBER = "Le numéro de transaction doit être une suite de chiffres";

    String NO_BALANCE = "You have no more money";

    String ACCOUNT_NUMBER_IS_EMPTY = "Le compte bancaire ne peut pas être vide";
    String ACCOUNT_NUMBER = "Le compte bancaire doit être une suite de chiffres";
}
