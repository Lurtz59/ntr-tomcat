package com.application.entrypoint.configuration;

public interface PageURL {
    String ALL = "/";

    String REGISTRATION = "/inscription";

    String LOGIN = "/connexion";
    String LOGIN_ERROR = "/connexion-erreur";
    String LOGOUT = "/deconnexion";

    String MY_SYNTHESIS = "/ma-synthese";
    String MY_SYNTHESIS_AUTHORIZATION = MY_SYNTHESIS+"/**";

    String TRANSFER = "/virement";
    String TRANSFER_AUTHORIZATION = TRANSFER +"/**";

    String CSS = "/css/**";
    String JS = "/js/**";
    String WEBJARS = "/webjars/**";
}
