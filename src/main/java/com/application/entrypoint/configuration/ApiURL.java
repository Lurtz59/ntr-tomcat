package com.application.entrypoint.configuration;

public interface ApiURL {

    String API = "/api";
    String API_AUTHORIZATION = API + "/**";

    String CLIENT = API+"/client";
    String OPERATION = API+"/operation";

    String IS_PAID = "/is-paid";
    String IS_REFUND = "/is-refund";
    String DEBIT = "/debit";
    String CREDIT = "/credit";
    String TRANSACTION = "/transaction";
    String INFOS = "/infos";
}
