package com.application.entrypoint.restcontroller;

import com.application.entrypoint.configuration.ApiURL;
import com.application.entrypoint.restcontroller.error.TransactionNotNumberException;
import com.application.service.OperationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.Collection;

@RestController
@RequestMapping(ApiURL.OPERATION)
public class OperationControllerJSON {

    @Autowired
    private OperationService operationService;

    @GetMapping(value = ApiURL.IS_PAID +"/{transaction}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean orderIsPaid(@PathVariable final String transaction) {
        validationAttributeTransaction(transaction);
        return operationService.isPaid(new BigInteger(transaction));
    }

    @GetMapping(value = ApiURL.IS_REFUND +"/{transaction}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean orderIsRefund(@PathVariable final String transaction) {
        validationAttributeTransaction(transaction);
        return operationService.isRefund(new BigInteger(transaction));
    }

    @GetMapping(value = ApiURL.TRANSACTION, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<BigInteger> getNumOfTransactions(@RequestParam(value = "compte") final String numberAccount, @RequestParam(value = "operation") final String operationName) {
        validationAttributeTransaction(numberAccount);
        return operationService.getNumOfTransactions(new BigInteger(numberAccount), operationName);
    }

    private void validationAttributeTransaction(@PathVariable String transaction) {
        if (!StringUtils.isNumeric(transaction)) {
            throw new TransactionNotNumberException();
        }
    }
}
