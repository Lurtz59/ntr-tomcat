package com.application.entrypoint.restcontroller.error;

import com.application.entrypoint.configuration.ErrorMessage;

public class ClientNotFoundException extends RuntimeException {

    public ClientNotFoundException() {
        super(ErrorMessage.CLIENT_NOT_FOUND);
    }

}
