package com.application.entrypoint.restcontroller.error;

import com.application.entrypoint.configuration.ErrorMessage;

public class AccountNotNumberException extends RuntimeException {

    public AccountNotNumberException() {
        super(ErrorMessage.ACCOUNT_NUMBER);
    }

}
