package com.application.entrypoint.restcontroller.error;

import com.application.entrypoint.configuration.ErrorMessage;

public class LasteNameIsEmptyException extends RuntimeException {

    public LasteNameIsEmptyException() {
        super(ErrorMessage.CLIENT_LAST_NAME_IS_EMPTY);
    }

}
