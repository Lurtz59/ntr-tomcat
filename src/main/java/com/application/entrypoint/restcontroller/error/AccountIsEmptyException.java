package com.application.entrypoint.restcontroller.error;

import com.application.entrypoint.configuration.ErrorMessage;

public class AccountIsEmptyException extends RuntimeException {

    public AccountIsEmptyException() {
        super(ErrorMessage.ACCOUNT_NUMBER_IS_EMPTY);
    }

}
