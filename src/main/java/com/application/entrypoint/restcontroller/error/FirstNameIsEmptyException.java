package com.application.entrypoint.restcontroller.error;

import com.application.entrypoint.configuration.ErrorMessage;

public class FirstNameIsEmptyException extends RuntimeException {

    public FirstNameIsEmptyException() {
        super(ErrorMessage.CLIENT_FIRST_NAME_IS_EMPTY);
    }

}
