package com.application.entrypoint.restcontroller.error;

import com.application.entrypoint.configuration.ErrorMessage;

public class NoBalanceException extends RuntimeException {

    public NoBalanceException() {
        super(ErrorMessage.NO_BALANCE);
    }

}
