package com.application.entrypoint.restcontroller.error;

import com.application.entrypoint.configuration.ErrorMessage;

public class TransactionNotFound extends RuntimeException {

    public TransactionNotFound() {
        super(ErrorMessage.TRANSACTION_NOT_FOUND);
    }

}
