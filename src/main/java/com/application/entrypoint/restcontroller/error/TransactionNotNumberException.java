package com.application.entrypoint.restcontroller.error;

import com.application.entrypoint.configuration.ErrorMessage;

public class TransactionNotNumberException extends RuntimeException {

    public TransactionNotNumberException() {
        super(ErrorMessage.TRANSACTION_NOT_NUMBER);
    }

}
