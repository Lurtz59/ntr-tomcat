package com.application.entrypoint.restcontroller;

import com.application.entrypoint.configuration.ApiURL;
import com.application.entrypoint.restcontroller.error.*;
import com.application.service.OperationService;
import com.application.service.dto.TransactionDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiURL.OPERATION)
public class OperationControllerXML {

    @Autowired
    private OperationService operationService;

    @PostMapping(value = ApiURL.CREDIT, produces=MediaType.APPLICATION_XML_VALUE)
    public void creditBankAccount(@RequestBody final TransactionDTO transactionDTO) {
        verificationClientTransactionDTO(transactionDTO);
        operationService.creditBankAccount(transactionDTO);
    }

    @PostMapping(value = ApiURL.DEBIT, produces = MediaType.APPLICATION_XML_VALUE)
    public void debitBankAccount(@RequestBody final TransactionDTO transactionDTO) {
        verificationClientTransactionDTO(transactionDTO);
        operationService.debitBankAccount(transactionDTO);
    }

    private void verificationClientTransactionDTO(final TransactionDTO transactionDTO)  {
        if(StringUtils.isEmpty(transactionDTO.getNumberAccount())) {
            throw new AccountIsEmptyException();
        }
        if(!StringUtils.isNumeric(transactionDTO.getNumberAccount())) {
            throw new AccountNotNumberException();
        }
    }
}
