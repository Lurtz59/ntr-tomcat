package com.application.entrypoint.restcontroller;

import com.application.entrypoint.restcontroller.error.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ClientNotFoundException.class)
    protected ResponseEntity<Object> handleClientNotFoundException(HttpServletRequest request, ClientNotFoundException ex, WebRequest req) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, req);
    }

    @ExceptionHandler(TransactionNotFound.class)
    protected ResponseEntity<Object> handleTransactionNotFound(HttpServletRequest request, TransactionNotFound ex, WebRequest req) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, req);
    }

    @ExceptionHandler(TransactionNotNumberException.class)
    protected ResponseEntity<Object> handleTransactionNotNumberException(HttpServletRequest request, TransactionNotNumberException ex, WebRequest req) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, req);
    }

    @ExceptionHandler(FirstNameIsEmptyException.class)
    protected ResponseEntity<Object> handleFirstNameIsEmpty(HttpServletRequest request, FirstNameIsEmptyException ex, WebRequest req) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, req);
    }

    @ExceptionHandler(LasteNameIsEmptyException.class)
    protected ResponseEntity<Object> handleLasteNameIsEmpty(HttpServletRequest request, LasteNameIsEmptyException ex, WebRequest req) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, req);
    }
}