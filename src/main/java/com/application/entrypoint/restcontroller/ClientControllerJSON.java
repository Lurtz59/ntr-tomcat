package com.application.entrypoint.restcontroller;

import com.application.entrypoint.configuration.ApiURL;
import com.application.entrypoint.restcontroller.error.TransactionNotNumberException;
import com.application.service.dto.ClientDTO;
import com.application.service.ClientService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;

@RestController
@RequestMapping(ApiURL.CLIENT)
public class ClientControllerJSON {

    @Autowired
    private ClientService clientService;

    @GetMapping(value = ApiURL.INFOS+"/{transaction}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ClientDTO getInfos(HttpServletResponse res, @PathVariable final String transaction) {
        if(!StringUtils.isNumeric(transaction)) {
            throw new TransactionNotNumberException();
        }
        return clientService.findByTransaction(new BigInteger(transaction));
    }
}
