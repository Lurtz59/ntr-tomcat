package com.application.persistance.entity;

import com.application.persistance.entity.type.AmountType;

import javax.persistence.*;

@Entity
public class Amount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Basic
    @Enumerated(EnumType.STRING)
    private AmountType type;

    @Basic
    private Double sum;

    @OneToOne(mappedBy = "amount")
    private Operation operation;

    public Amount() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AmountType getType() {
        return type;
    }

    public void setType(AmountType type) {
        this.type = type;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
}
