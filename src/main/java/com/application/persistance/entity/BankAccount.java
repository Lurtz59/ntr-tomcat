package com.application.persistance.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

@Entity
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Basic
    private String wording;

    @Basic
    @Column(name = "number_account", unique = true)
    private BigInteger numberAccount;

    @Basic
    @Column(unique = true)
    private String rib;

    @Basic
    private Double balance;

    @Column(name = "is_enabled")
    private boolean isEnabled;

    @OneToMany(mappedBy = "bankAccount", fetch = FetchType.LAZY) // inverse side: it has a mappedBy attribute, and can't decide how the association is mapped, since the other side already decided it.
    private List<Operation> operations ;

    @OneToOne(mappedBy = "bankAccount")
    private Client client;

    public BankAccount() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWording() {
        return wording;
    }

    public void setWording(String wording) {
        this.wording = wording;
    }

    public BigInteger getNumberAccount() {
        return numberAccount;
    }

    public void setNumberAccount(BigInteger numberAccount) {
        this.numberAccount = numberAccount;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
