package com.application.persistance.entity;

import com.application.persistance.entity.type.MeansOfPayement;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Entity
public class Operation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "operation_name", nullable = false)
    private String operationName;

    @Column(name = "creation_date", nullable = false, columnDefinition="DATETIME")
    private Date creationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "means_of_payment", nullable = false)
    private MeansOfPayement meansOfPayement;

    @Column(name = "transaction_number")
    private BigInteger transactionNumber;

    @ManyToOne
    @JoinTable(name =   "bank_acount_operation", joinColumns = @JoinColumn(name="operation_id"),
            inverseJoinColumns= @JoinColumn(name="bank_account_id"))
    private BankAccount bankAccount;

    @Column(name = "is_enabled")
    private boolean isEnabled;

    @OneToOne
    private Amount amount;

    public Operation() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public MeansOfPayement getMeansOfPayement() {
        return meansOfPayement;
    }

    public void setMeansOfPayement(MeansOfPayement meansOfPayement) {
        this.meansOfPayement = meansOfPayement;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public BigInteger getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(BigInteger transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }
}
