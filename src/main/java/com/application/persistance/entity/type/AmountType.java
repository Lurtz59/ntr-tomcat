package com.application.persistance.entity.type;

public enum  AmountType {
    CREDIT,
    DEBIT
}
