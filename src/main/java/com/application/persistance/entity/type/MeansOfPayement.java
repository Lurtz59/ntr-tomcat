package com.application.persistance.entity.type;

public enum MeansOfPayement {
    BANK_CARD,
    SPECIES,
    BANK_CHECK
}
