package com.application.persistance.repository;

import com.application.persistance.entity.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Integer> {

    BankAccount getBankAccountByNumberAccount(final BigInteger numberAccount);

}
