package com.application.persistance.repository;

import com.application.persistance.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {

    Client findByLogin(final String login);

    @Query(value = "SELECT c FROM Client c JOIN c.bankAccount.operations as operations WHERE operations.transactionNumber = ?1 ")
    Client findByTransaction(final BigInteger transaction);

    Client findByBankAccount_NumberAccount(final BigInteger numberAccount);
}
