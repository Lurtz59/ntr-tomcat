package com.application.persistance.repository;

import com.application.persistance.entity.Operation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Collection;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Integer> {

    Collection<Operation> findByBankAccount_Client_Login(final String login);

    Operation findByTransactionNumber(final BigInteger transactionNumber);

    Collection<Operation> findByBankAccount_NumberAccountAndOperationName(final BigInteger numberAccount, final String operationName);
}
