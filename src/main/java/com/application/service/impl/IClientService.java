package com.application.service.impl;

import com.application.entrypoint.restcontroller.error.ClientNotFoundException;
import com.application.persistance.entity.Amount;
import com.application.persistance.entity.BankAccount;
import com.application.persistance.entity.Client;
import com.application.persistance.entity.Operation;
import com.application.persistance.entity.type.AmountType;
import com.application.persistance.entity.type.MeansOfPayement;
import com.application.persistance.repository.*;
import com.application.service.dto.AgencyDTO;
import com.application.service.dto.ClientDTO;
import com.application.service.dto.RegistrationDTO;
import com.application.service.dto.mapper.ClientMapper;
import com.application.service.ClientService;
import com.application.service.dto.mapper.InscriptionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.*;

@Service
public class IClientService implements ClientService {

    public static final String PRINCIPAL_ACCOUNT = "Compte principal";
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AgencyRepository agencyRepository;

    @Autowired
    private AmountRepository amountRepository;

    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    private ISecurityService securityService;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public ClientDTO findByTransaction(BigInteger transaction) {
        final Client client = clientRepository.findByTransaction(transaction);
        if(client == null) {
            throw new ClientNotFoundException();
        }
        return getClientDTOWithAgency(client);
    }

    @Override
    public ClientDTO getLoggedUser() {
        final Client client = (Client) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ClientMapper.entityToDTO(client);
    }

    private ClientDTO getClientDTOWithAgency(final Client client) {
        final ClientDTO clientDTO = new ClientDTO();
        clientDTO.setLastName(client.getLastName());
        clientDTO.setFirstName(client.getFirstName());

        final AgencyDTO agencyDTO = new AgencyDTO();
        agencyDTO.setCity(client.getAgency().getCity());
        agencyDTO.setName(client.getAgency().getName());
        clientDTO.setAgency(agencyDTO);

        return clientDTO;
    }


    @Override
    public void autoLogin(String username, String password) {
        UserDetails userDetails = securityService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());

        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        }
    }

    @Override
    public void registerNewClientAccount(RegistrationDTO inscriptionDTO) {
        // create client
        inscriptionDTO.setPassword(bCryptPasswordEncoder.encode(inscriptionDTO.getPassword()));
        final Client client = InscriptionMapper.dtoToClientEntity(inscriptionDTO);
        // create bank account
        BankAccount bankAccount = new BankAccount();
        // create operation
        final Random rand = new Random();
        Operation operation = createOperation(rand);
        // uinit bank account
        initBankAccount(bankAccount, rand, operation);
        // end creation client
        client.setAgency(agencyRepository.findById(rand.nextInt(2) + 1).orElse(null));
        bankAccount = bankAccountRepository.save(bankAccount);
        client.setBankAccount(bankAccount);
        client.getBankAccount().getOperations().get(0).setBankAccount(bankAccount);
        client.setEnabled(true);
        clientRepository.save(client);
    }

    private Operation createOperation(Random rand) {
        final Operation operation = new Operation();
        operation.setOperationName("Cadeau de bienvenue");
        operation.setCreationDate(new Date());
        operation.setMeansOfPayement(MeansOfPayement.BANK_CARD);
        operation.setTransactionNumber(new BigInteger(32, rand));
        operation.setEnabled(true);
        // create amount and save
        final Amount amount = new Amount();
        amount.setType(AmountType.CREDIT);
        amount.setSum(1000D);
        operation.setAmount(amountRepository.save(amount));
        return operation;
    }

    private void initBankAccount(BankAccount bankAccount, Random rand, Operation operation) {
        bankAccount.setNumberAccount(new BigInteger(42, rand));
        bankAccount.setBalance(1000D);
        bankAccount.setEnabled(true);
        bankAccount.setWording(PRINCIPAL_ACCOUNT);
        bankAccount.setOperations(Collections.singletonList(operationRepository.save(operation)));
        bankAccount.setRib("FR001234512345123"+rand.nextInt(100000));
    }

    @Override
    public boolean loginExist(String login) {
        return clientRepository.findByLogin(login) != null;
    }
}
