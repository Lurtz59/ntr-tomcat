package com.application.service.impl;

import com.application.persistance.entity.BankAccount;
import com.application.persistance.repository.BankAccountRepository;
import com.application.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class IBankAccountService implements BankAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;


    @Override
    public Double getBalanceByNumberAccount(BigInteger numberAccount) {
        BankAccount bankAccount = bankAccountRepository.getBankAccountByNumberAccount(numberAccount);
        return bankAccount.getBalance();
    }
}
