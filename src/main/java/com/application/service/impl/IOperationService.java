package com.application.service.impl;

import com.application.entrypoint.restcontroller.error.ClientNotFoundException;
import com.application.entrypoint.restcontroller.error.NoBalanceException;
import com.application.entrypoint.restcontroller.error.TransactionNotFound;
import com.application.persistance.entity.Amount;
import com.application.persistance.entity.Client;
import com.application.persistance.entity.Operation;
import com.application.persistance.entity.type.AmountType;
import com.application.persistance.entity.type.MeansOfPayement;
import com.application.persistance.repository.AmountRepository;
import com.application.persistance.repository.ClientRepository;
import com.application.persistance.repository.OperationRepository;
import com.application.service.OperationService;
import com.application.service.dto.OperationDTO;
import com.application.service.dto.TransactionDTO;
import com.application.service.dto.mapper.OperationMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class IOperationService implements OperationService {

    public static final String REFUND_OPERATION = "Remboursement du produit sur Coffee machine store";
    public static final String BUY_OPERATION = "Achat d'un produit sur Coffee machine store";

    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    private AmountRepository amountRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Collection<OperationDTO> findByClient(String login) {
        return OperationMapper.entityToDTO(operationRepository.findByBankAccount_Client_Login(login));
    }

    @Override
    public Collection<BigInteger> getNumOfTransactions(final BigInteger numberAccount, final String operationName) {
        final Collection<Operation> operations = operationRepository.findByBankAccount_NumberAccountAndOperationName(numberAccount, operationName);
        if(CollectionUtils.isEmpty(operations))  {
            throw new TransactionNotFound();
        }
        return operations.stream().map(Operation::getTransactionNumber).collect(Collectors.toList());
    }

    @Override
    public boolean isPaid(BigInteger transaction) {
        Operation operation = operationRepository.findByTransactionNumber(transaction);
        if(operation == null) {
            throw new TransactionNotFound();
        }
        return AmountType.DEBIT.equals(operation.getAmount().getType()) && operation.isEnabled();
    }

    @Override
    public boolean isRefund(BigInteger transaction) {
        Operation operation = operationRepository.findByTransactionNumber(transaction);
        if(operation == null) {
            throw new TransactionNotFound();
        }
        return AmountType.CREDIT.equals(operation.getAmount().getType()) && operation.isEnabled();
    }

    @Override
    public void creditBankAccount(final TransactionDTO transactionDTO) {
        final Client client = clientRepository.findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()));
        if(client == null) {
            throw new ClientNotFoundException();
        }
        Operation operation = getOperationAndSaveAmount(transactionDTO, client, AmountType.CREDIT);
        client.getBankAccount().setBalance(client.getBankAccount().getBalance() + operation.getAmount().getSum());
        operationRepository.save(operation);
    }

    @Override
    public void debitBankAccount(final TransactionDTO transactionDTO) {
        final Client client = clientRepository.findByBankAccount_NumberAccount(new BigInteger(transactionDTO.getNumberAccount()));
        if(client == null) {
            throw new ClientNotFoundException();
        }
        if(client.getBankAccount().getBalance() == 0) {
            throw new NoBalanceException();
        }
        Operation operation = getOperationAndSaveAmount(transactionDTO, client, AmountType.DEBIT);
        client.getBankAccount().setBalance(client.getBankAccount().getBalance() - operation.getAmount().getSum());
        operationRepository.save(operation);
    }

    private Operation getOperationAndSaveAmount(TransactionDTO transactionDTO, Client client, AmountType amountType) {
        Operation operation = new Operation();
        Amount amount = new Amount();
        amount.setSum(transactionDTO.getSum());
        amount.setType(amountType);
        operation.setAmount(amountRepository.save(amount));
        operation.setEnabled(true);
        operation.setCreationDate(new Date());
        operation.setMeansOfPayement(MeansOfPayement.BANK_CARD);
        if(AmountType.CREDIT.equals(amountType)) {
            operation.setOperationName(REFUND_OPERATION);
        } else {
            operation.setOperationName(BUY_OPERATION);
        }
        Random rand = new Random();
        operation.setTransactionNumber(new BigInteger(32, rand));
        operation.setBankAccount(client.getBankAccount());
        return operation;
    }
}
