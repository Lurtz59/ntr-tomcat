package com.application.service;

import com.application.service.dto.OperationDTO;
import com.application.service.dto.TransactionDTO;

import java.math.BigInteger;
import java.util.Collection;

public interface OperationService {

    Collection<OperationDTO> findByClient(final String login);

    Collection<BigInteger> getNumOfTransactions(final BigInteger numberAccount, final String operationName);

    boolean isPaid(final BigInteger transaction);

    boolean isRefund(final BigInteger transaction);

    void creditBankAccount(final TransactionDTO transactionDTO);

    void debitBankAccount(final TransactionDTO clientTransactionDTO);
}
