package com.application.service;

import com.application.service.dto.ClientDTO;
import com.application.service.dto.RegistrationDTO;

import java.math.BigInteger;

public interface ClientService {

    boolean loginExist(String login);

    ClientDTO findByTransaction(final BigInteger transaction);

    ClientDTO getLoggedUser();

    void registerNewClientAccount(RegistrationDTO inscriptionDTO);

    void autoLogin(String username, String password);
}
