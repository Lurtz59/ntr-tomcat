package com.application.service;

import java.math.BigInteger;

public interface BankAccountService {

    Double getBalanceByNumberAccount(BigInteger numberAccount) ;
}
