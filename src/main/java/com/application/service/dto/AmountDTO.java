package com.application.service.dto;

import com.application.persistance.entity.type.AmountType;

import java.io.Serializable;

public class AmountDTO implements Serializable {

    private AmountType type;
    private Double sum;

    public AmountDTO() {}

    public AmountType getType() {
        return type;
    }

    public void setType(AmountType type) {
        this.type = type;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }
}
