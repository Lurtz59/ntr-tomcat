package com.application.service.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;

public class BankAccountDTO implements Serializable {

    private String wording;
    private BigInteger numberAccount;
    private String rib;
    private Double balance;

    public BankAccountDTO() {}

    public String getWording() {
        return wording;
    }

    public void setWording(String wording) {
        this.wording = wording;
    }

    public BigInteger getNumberAccount() {
        return numberAccount;
    }

    public void setNumberAccount(BigInteger numberAccount) {
        this.numberAccount = numberAccount;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
