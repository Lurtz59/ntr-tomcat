package com.application.service.dto.mapper;

import com.application.persistance.entity.Client;
import com.application.service.dto.ClientDTO;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

public class ClientMapper {

    public static ClientDTO entityToDTO(Client client) {
        ClientDTO clientDTO = new ClientDTO();
        if(StringUtils.isNotBlank(client.getLogin())) {
            clientDTO.setLogin(client.getLogin());
        }
        if(client.getAgency() != null) {
            clientDTO.setAgency(AgencyMapper.entityToDTO(client.getAgency()));
        }
        if(client.getBankAccount() != null) {
            clientDTO.setBankAccount(BankAccountMapper.entityToDTO(client.getBankAccount()));
        }
        if(StringUtils.isNotBlank(client.getFirstName())) {
            clientDTO.setFirstName(client.getFirstName());
        }
        if(StringUtils.isNotBlank(client.getLastName())) {
            clientDTO.setLastName(client.getLastName());
        }
        return clientDTO;
    }
}
