package com.application.service.dto.mapper;

import com.application.persistance.entity.Operation;
import com.application.service.dto.OperationDTO;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class OperationMapper {

    public static OperationDTO entityToDTO(Operation operation) {
        OperationDTO operationDTO = new OperationDTO();
        if(operation.getCreationDate() != null) {
            operationDTO.setCreationDate(operation.getCreationDate());
        }
        if(operation.getMeansOfPayement() != null) {
            operationDTO.setMeansOfPayement(operation.getMeansOfPayement());
        }
        if(StringUtils.isNotBlank(operation.getOperationName())) {
            operationDTO.setOperationName(operation.getOperationName());
        }
        if(operation.getTransactionNumber() != null) {
            operationDTO.setTransactionNumber(operation.getTransactionNumber());
        }
        if(operation.getAmount() != null) {
            operationDTO.setAmount(AmountMapper.entityToDTO(operation.getAmount()));
        }
        return operationDTO;
    }

    public static Collection<OperationDTO> entityToDTO(Collection<Operation> operations) {
        Collection<OperationDTO> operationDTOS = new ArrayList<>();
        for(Operation operation : operations) {
            operationDTOS.add(entityToDTO(operation));
        }
        return operationDTOS;
    }
}
