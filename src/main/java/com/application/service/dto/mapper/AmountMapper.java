package com.application.service.dto.mapper;

import com.application.persistance.entity.Amount;
import com.application.service.dto.AmountDTO;

public class AmountMapper {

    public static AmountDTO entityToDTO(Amount amount) {
        AmountDTO amountDTO = new AmountDTO();
        if(amount.getSum() != null) {
            amountDTO.setSum(amount.getSum());
        }
        if(amount.getType() != null) {
            amountDTO.setType(amount.getType());
        }
        return amountDTO;
    }
}
