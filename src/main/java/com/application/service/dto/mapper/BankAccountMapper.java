package com.application.service.dto.mapper;

import com.application.persistance.entity.BankAccount;
import com.application.service.dto.BankAccountDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

public class BankAccountMapper {

    public static BankAccountDTO entityToDTO(BankAccount bankAccount) {
        BankAccountDTO bankAccountDTO = new BankAccountDTO();
        if(bankAccount.getBalance() != null) {
            bankAccountDTO.setBalance(bankAccount.getBalance());
        }
        if(bankAccount.getNumberAccount() != null) {
            bankAccountDTO.setNumberAccount(bankAccount.getNumberAccount());
        }
        if(StringUtils.isNotBlank(bankAccount.getRib())) {
            bankAccountDTO.setRib(bankAccount.getRib());
        }
        if(StringUtils.isNotBlank(bankAccount.getWording())) {
            bankAccountDTO.setWording(bankAccount.getWording());
        }
        return bankAccountDTO;
    }
}
