package com.application.service.dto.mapper;

import com.application.persistance.entity.Agency;
import com.application.service.dto.AgencyDTO;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

public class AgencyMapper {

    public static AgencyDTO entityToDTO(Agency agency) {
        AgencyDTO agencyDTO = new AgencyDTO();
        if(StringUtils.isNotBlank(agency.getName())) {
            agencyDTO.setName(agency.getName());
        }
        if(StringUtils.isNotBlank(agency.getCity())) {
            agencyDTO.setCity(agency.getCity());
        }
        return agencyDTO;
    }
}
