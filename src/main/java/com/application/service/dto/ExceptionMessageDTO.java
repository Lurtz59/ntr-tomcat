package com.application.service.dto;

public class ExceptionMessageDTO {
    private String message;
    private String className;
    private String path;
    private String date;

    public ExceptionMessageDTO(String message, String className, String path, String date) {
        this.message = message;
        this.className = className;
        this.path = path;
        this.date = date;
    }
}