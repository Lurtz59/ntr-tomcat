package com.application.service.dto;

import com.application.persistance.entity.type.MeansOfPayement;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class OperationDTO implements Serializable {

    private String operationName;
    private Date creationDate;
    private MeansOfPayement meansOfPayement;
    private AmountDTO amount;
    private BigInteger transactionNumber;

    public OperationDTO() {}

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public MeansOfPayement getMeansOfPayement() {
        return meansOfPayement;
    }

    public void setMeansOfPayement(MeansOfPayement meansOfPayement) {
        this.meansOfPayement = meansOfPayement;
    }

    public AmountDTO getAmount() {
        return amount;
    }

    public void setAmount(AmountDTO amount) {
        this.amount = amount;
    }

    public BigInteger getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(BigInteger transactionNumber) {
        this.transactionNumber = transactionNumber;
    }
}
