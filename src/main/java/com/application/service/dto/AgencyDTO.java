package com.application.service.dto;

import java.io.Serializable;

public class AgencyDTO implements Serializable {

    private String name;
    private String City;

    public AgencyDTO () {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }
}
