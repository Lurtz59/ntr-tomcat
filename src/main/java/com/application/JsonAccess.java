package com.application;

import com.application.service.dto.ClientDTO;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.MarshallerProperties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

public class JsonAccess {

    public static void main(String[] args) throws IOException, JAXBException {
        switch (args[0]) {
            case "operation":
                getOperation();
                break;
            case "client":
                getClientInfos();
                break;
            case "refund":
                getRefund();
                break;
            case "paid":
                getPaid();
                break;
        }
    }

    public static void getOperation() throws IOException, JAXBException {
        //appel client infos

        String uri = "http://localhost:8081/api/operation/transaction?operation=Remboursement%20Amazon&compte=1478541452158";
        URL url = new URL(uri);
        HttpURLConnection connection = (HttpURLConnection)
                url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");

        InputStream inputStream = connection.getInputStream();
        byte[] res = new byte[2048];
        int i = 0;
        StringBuilder response = new StringBuilder();
        while ((i = inputStream.read(res)) != -1) {
            response.append(new String(res, 0, i));
        }
        inputStream.close();
        System.out.println("Reponse = " + response.toString());
    }

    //Test de l'appel permettant de récupérer les infos du client
    public static void getClientInfos() throws IOException, JAXBException {

        String uri = "http://localhost:8081/api/client/infos/9234567801";
        URL url = new URL(uri);
        HttpURLConnection connection = (HttpURLConnection)
                url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");

        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("eclipselink.media-type", "application/json");
        properties.put(MarshallerProperties.JSON_INCLUDE_ROOT, false);
        JAXBContext ctx = JAXBContextFactory.createContext(new Class[]
                {ClientDTO.class}, properties);
        Unmarshaller jsonUnmarshaller = ctx.createUnmarshaller();
        StreamSource jsonStream = new StreamSource(connection.getInputStream());
        ClientDTO client = jsonUnmarshaller.unmarshal(jsonStream,
                ClientDTO.class).getValue();
        connection.disconnect();
        System.out.println("Le prenom est :"+ client.getFirstName() + "\nLe nom est :" +client.getLastName() + "\nLe le nom de l'agence est :" + client.getAgency().getName()+ "\nLa ville de l'agence est :" +client.getAgency().getCity());
    }

    //Test de l'appel de la transaction de remboursement
    public static void getRefund() throws IOException, JAXBException {

        String uri = "http://localhost:8081/api/operation/is-refund/4234567801";
        URL url = new URL(uri);
        HttpURLConnection connection = (HttpURLConnection)
                url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");
        InputStream inputStream = connection.getInputStream();
        byte[] res = new byte[2048];
        int i = 0;
        StringBuilder response = new StringBuilder();
        while ((i = inputStream.read(res)) != -1) {
            response.append(new String(res, 0, i));
        }
        inputStream.close();
        System.out.println("Reponse = " + response.toString());
        System.out.println("La commande a été remboursée");
    }

    //test de l'appel d'une transaction de paiement
    public static void getPaid() throws IOException, JAXBException {
        String uri = "http://localhost:8081/api/operation/is-paid/9234567801";
        URL url = new URL(uri);
        HttpURLConnection connection = (HttpURLConnection)
                url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");
        InputStream inputStream = connection.getInputStream();
        byte[] res = new byte[2048];
        int i = 0;
        StringBuilder response = new StringBuilder();
        while ((i = inputStream.read(res)) != -1) {
            response.append(new String(res, 0, i));
        }
        inputStream.close();
        System.out.println("Reponse = " + response.toString());
        System.out.println("La commande à été payée");
    }
}