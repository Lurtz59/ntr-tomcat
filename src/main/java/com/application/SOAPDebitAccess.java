package com.application;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class SOAPDebitAccess {


    public static void main(String[] args) throws IOException, JAXBException {

        try {
            String soapBody = "<soapenv:Envelope \n" +
                    "   xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" \n" +
                    "   xmlns:ws=\"http://controller.application.com/\">\n" +
                    " <soapenv:Header/>\n" +
                    " <soapenv:Body>\n" +
                    "    <ws:debit>\n" +
                    "       <arg0>1478541452158</arg0>\n" +
                    "       <arg1>500</arg1>\n" +
                    "    </ws:debit>\n" +
                    " </soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            URL url = new URL("http://localhost:8080/ntr-widfly/operation");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Set timeout as per needs
            connection.setConnectTimeout(20000);
            connection.setReadTimeout(20000);

            // Set DoOutput to true if you want to use URLConnection for output.
            // Default is false
            connection.setDoOutput(true);

            connection.setUseCaches(true);
            connection.setRequestMethod("POST");

            // Set Headers
            connection.setRequestProperty("Accept", "application/xml");
            connection.setRequestProperty("Content-Type", "text/xml");
            connection.setRequestProperty("Data-Type", "xml");

            // Write XML
            OutputStream outputStream = connection.getOutputStream();

            byte[] b = soapBody.getBytes("UTF-8");
            outputStream.write(b);
            outputStream.flush();
            outputStream.close();

            // Read XML
            InputStream inputStream = connection.getInputStream();
            byte[] res = new byte[2048];
            int i = 0;
            StringBuilder response = new StringBuilder();
            while ((i = inputStream.read(res)) != -1) {
                response.append(new String(res, 0, i));
            }
            inputStream.close();

            System.out.println("SOAP Debit WORK");
            System.out.println("Reponse = " + response.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
